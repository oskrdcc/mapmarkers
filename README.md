## React Native ViewMap App

#### To run the project

* npm install
* npm start
* run an android emulator or connect a device to run the app
(in another terminal)
* react-native run-android

### About this app

This the simplest app to import and use GMaps on react-native project. It is based on an old YouTube video (2017) with some adaptations in order to get the same outcome now (2019).

* Video by Academind => [React Native Tutorial for Beginners - Getting Started.](https://www.youtube.com/watch?v=6ZnfsJ6mM5c)

> I use a library to use GMaps and another one to get the geo-points through GPS.
> * [Gmaps lib](https://github.com/react-native-community/react-native-maps/blob/master/docs/installation.md)
> * [Geolocation lib](https://github.com/Agontuk/react-native-geolocation-service)

As a part of the build process on Android after SDK v.23 (Android M), it's a mandatory requirement [ask for permission](https://facebook.github.io/react-native/docs/permissionsandroid) to use location in a app.


* React native version 0.61

import React from 'react';
import { View, Button, StyleSheet } from 'react-native';

const fetchLocation = props => {
  return (
    <View style = {styles.container}>
      <Button title = "Show prev locations" onPress={props.onPrintMarkers}/>
      <Button title ="Get Location" onPress={props.onGetLocation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container : {
    flex : 1,
    flexDirection : 'column',
    marginBottom : 50,
    marginTop : 50,
    justifyContent : 'space-between'
  }
});

export default fetchLocation;

import React from 'react';
import { View, StyleSheet } from 'react-native';
import MapView from 'react-native-maps';

const usersMap = props => {
  let userLocationMarker = null;

  if(props.userLocation) {
    userLocationMarker = <MapView.Marker coordinate={props.userLocation} />
  }
  const userMarkers = props.userPlaces.map(userPlace => (
    <MapView.Marker coordinate={userPlace} key={userPlace.id} />
  ));

  return(
    <View style={styles.mapContainer}>
      <MapView
        initialRegion = {{
          latitude: 20.2465,
          longitude: -87.4640,
          latitudeDelta: 0.0822,
          longitudeDelta: 0.0421,
          }}
          region = {props.userLocation}
        style={styles.map} >
          {userLocationMarker}
          {userMarkers}
        </MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  mapContainer : {
    width : '100%',
    height : 300
  },
  map : {
    width : '100%',
    height : '100%'
  }
});


export default usersMap;

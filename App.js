/**
 * Maps App
 *
 */

import React, { Component } from 'react';
import { View, Text, StyleSheet, PermissionsAndroid } from 'react-native';
import Geolocation from 'react-native-geolocation-service';

import FetchLocation from './components/FetchLocation';
import UsersMap from './components/UsersMap';

const DEBUG = false;

async function requestCoordinatesPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title : 'GPS Permission',
        message : 'Permission to get GPS coordinates it is needed',
        buttonNeutral : 'Remind me later',
        buttonNegative : 'No',
        buttonPositive : 'Ok',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can continue');
    }
    else {
      console.log('No way to this will work');
    }
  } catch (err) {
    console.warn(err);
  }
}


export default class App extends Component {
  constructor() {
    super();
    requestCoordinatesPermission();
    this.state = {
      userLocation : null,
      userPlaces : []
    }
  }


  getUserLocationHandler = () => {
    Geolocation.getCurrentPosition(position => {
      if (DEBUG) console.log(position);
      this.setState({
        userLocation : {
          latitude : position.coords.latitude,
          longitude : position.coords.longitude,
          latitudeDelta: 0.0822,
          longitudeDelta: 0.0421
        }
      });
      fetch('https://YOUR_REALTIME_DATABASE_NAME.firebaseio.com/locations.json', {
        method : 'POST',
        body : JSON.stringify({
          latitude : position.coords.latitude,
          longitude : position.coords.longitude
        })
      })
      .then(res => console.log(res))
      .catch(err => console.log(err));
      },
      err => {
        console.log(err);
      },
      { enableHighAccuracy : true, timeout : 15000, maximumAge : 10000 }
    );
  }

  getStoredPositions = () => {
    fetch('https://YOUR_REALTIME_DATABASE_NAME.firebaseio.com/locations.json', {
      method : 'GET'
    })
    .then(res => res.json())
    .then(parsedRes => {
      const placesArray = [];
      for(const key in parsedRes) {
        placesArray.push({
          latitude : parsedRes[key].latitude,
          longitude : parsedRes[key].longitude,
          id : key
        });
      }
      this.setState({
        userPlaces : placesArray
      });
    })
    .catch(err => console.log(err));
  }

  render () {
    return (
        <View style={styles.center}>
          <View style={styles.topBar}>
            <Text style={styles.headerLabel}>
              Spots' collector
            </Text>
          </View>
          <FetchLocation
            onGetLocation={this.getUserLocationHandler}
            onPrintMarkers={this.getStoredPositions}
          />
          <UsersMap
            userLocation={this.state.userLocation}
            userPlaces={this.state.userPlaces}
          />
        </View>
      );
  }
}

const styles = StyleSheet.create({
  center : {
    flex : 1,
    justifyContent : "center", // Vertical centred
    alignItems : "center",   //Horizontal centred
    marginBottom : 25
  },
  topBar : {
    width : '100%',
    height : 70,
    backgroundColor : 'cornflowerblue',
    justifyContent : 'center'
  },
  headerLabel : {
    color : 'white',
    fontSize : 22,
    fontWeight : 'bold',
    marginLeft : 10
  }
});
